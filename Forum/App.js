import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import {  createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import registerScreens from './src/components/screens.js';
import { Provider } from "react-redux";
import rootSaga from './src/sagas/rootSaga';
import * as reducers from './src/reducers/index';
import * as appActions from './src/actions';

const sagaMiddleWare = createSagaMiddleware();
const reducer = combineReducers(reducers);
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleWare)
);

sagaMiddleWare.run(rootSaga);
registerScreens(store, Provider);


export default class App extends Component {

    constructor(props) {
        super(props);
        store.subscribe(this.onStoreUpdate.bind(this));
        store.dispatch(appActions.appInitialized());
    }

    onStoreUpdate() {
        let {root} = store.getState().root;

        // handle a root change
        // if your app doesn't change roots in runtime, you can remove onStoreUpdate() altogether
        if (this.currentRoot != root) {
            this.currentRoot = root;
            this.startApp(root);
        }
    }

    startApp(root) {
        switch (root) {
            case 'login':
                Navigation.startSingleScreenApp({
                    screen: {
                        screen: 'Forum.List', // unique ID registered with Navigation.registerScreen
                        title: 'Post Forum', // title of the screen as appears in the nav bar (optional)
                        navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                        navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                    },
                });
                return;

        }
    }
}//end of class


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
