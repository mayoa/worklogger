
const initialState = {
    post: [ ],
    details: { },
    loading: false,
    error: false,

};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUESTED_DATA':
      return {
          post: null,
          loading: true,
          error: false,
      };
      case 'REQUESTED_DATA_SUCCEEDED':
      return {
          post: action.post,
          loading: false,
          error: false,
      };
     case 'REQUESTED_DATA_FAILED':
          return {
              post: null,
              loading: false,
              error: true,
          };
      case 'FETCHED_DETAILS_SUCCEEDED':
          return{
              details: action.details,
              loading: false,
              error: false
          };
      case 'FETCHED_DETAILS_FAILED':
          return{
              details: null,
              loading: false,
              error: false
          };
    default:
      return state;
  }
};

export const getPostState = state =>  state.post;
export const getSelectedId = state => state.post.id;
export const getDetails = state => state.details;
