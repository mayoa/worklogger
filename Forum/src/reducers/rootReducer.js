import { combineReducers } from 'redux'
import {
 GET_ALL_POSTS,
 RECEIVE_ALL_POSTS,
 API_CALL_ERROR,
} from '../actions'

const initialState = {
    posts: null,
    error: null
}

export function rootReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_POSTS:
      return { ...state,  error: null };
    case RECEIVE_ALL_POSTS:
      return { ...state, posts: action.posts };
    case API_CALL_ERROR:
      return { ...state, posts: null, error: action.error };
    default:
      return state;
  }
}



