import { dataReducer }from './dataReducer';
import { navigation }from './navigation';
import { combineReducers } from 'redux';


export {
  dataReducer,
  navigation
}
