import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import ListScreen from '../components/listScreen.js';
import { fetchData } from '../actions';



const  mapStateToProps = (state) => {
    return{
        post: state.post,
        loading: state.loading,
        error:state.error,
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        onRequestPost: () => dispatch(fetchData())
   };
};


export default connect(mapStateToProps, mapDispatchToProps)(ListScreen)
