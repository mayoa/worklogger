const REQUESTED_DATA = 'REQUESTED_DATA';
const REQUESTED_DATA_SUCCEEDED = 'REQUESTED_DATA_SUCCEEDED';
const REQUESTED_DATA_FAILED = 'REQUESTED_DATA_FAILED';
const ROOT_CHANGED = 'ROOT_CHANGED';

export const changeAppRoot = (root) => {
  return { type: 'ROOT_CHANGED', root };
};

/*export function* appInitialized() {
  return async function(dispatch, getState) {
    // since all business logic should be inside redux actions
    // this is a good place to put your app initialization code
    dispatch(changeAppRoot('login'));
  };
}*/

export function appInitialized() {
    return function action(dispatch) {
        dispatch({type: 'ROOT_CHANGED', root: 'login'})
    }
}

export const requestData = () => {
    return { type : 'REQUESTED_DATA' };
};

export const  requestDataSuccess = (data) => {
    return { type: 'REQUESTED_DATA_SUCCEEDED', post: data}
};

export const requestDataError = () => {
  return { type: 'REQUESTED_DATA_FAILED'}
};

export const fetchData = () => {
    return { type: 'FETCHED_DATA'}
};

export const fetchDetails = (id) => {
  return {type: 'FETCHED_DETAILS' ,id}

};
export const  requestDetailSuccess = (data) => {
    return { type: 'FETCHED_DETAILS_SUCCEEDED', details: data}
};

export const requestDetailError = () => {
    return {type: 'FETCHED_DETAILS_FAILED'}
};

export const createPost = (subject, body) =>{
    return {type: 'CREATE_POST' , subject, body}
}