import { takeLatest, put, call, fork}  from 'redux-saga/effects';
import {requestData, requestDataSuccess, requestDataError, requestDetailSuccess, requestDetailError, changeAppRoot } from '../actions';



function* createPost(action){
    try{
        const POST_ENDPOINT = 'https://jsonplaceholder.typicode.com/posts/';

        const eachData = yield call (() => {
            return fetch(POST_ENDPOINT, {
                method: 'POST',
                body: JSON.stringify({
                     title: action.subject,
                     body: action.body,
                     userId: 1
                 }),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
           }).then(response => response.json())
          }
        );

       yield put(requestDetailSuccess(eachData));

    }catch(error){
      yield put(requestDetailError());
    }
}

function* getPostDetails(action) {
    try {
        const POST_ENDPOINT = 'https://jsonplaceholder.typicode.com/posts/' + action.id;

        const eachData = yield call(() => {
                return fetch(POST_ENDPOINT)
                    .then(response => response.json())
            }
        );

        yield put(requestDetailSuccess(eachData));
    }catch(error){
        yield put(requestDetailError());
    }
}

function* postAll() {
    try{

      yield put(requestData());


      const allData = yield call (() => {
          return fetch('https://jsonplaceholder.typicode.com/posts')
              .then(res => res.json())
        }
      );

       yield put(requestDataSuccess(allData));

    }catch (error){
        yield put (requestDataError());
    }
}


/*function* appInitialized() {
    const screen = 'login';
    yield put(changeAppRoot(screen));

  /*return async function(dispatch, getState) {
    // since all business logic should be inside redux actions
    // this is a good place to put your app initialization code
    dispatch(changeAppRoot('login'));
  };
}*/

export default function* rootSaga() {
    //yield  fork(appInitialized);
    yield  takeLatest( 'FETCHED_DATA' , postAll);
    yield  takeLatest( 'FETCHED_DETAILS' , getPostDetails);
    yield  takeLatest('CREATE_POST', createPost)
}
