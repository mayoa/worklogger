import React, { Component } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class Posts extends Component {
  render() {
    return (
      <View>
        <Button
         onPress={this.props.onRequestPost()}
         tile={"Get Posts"}
         />
          {this.props.posts.map((post) => {
               <Text> {post.title} </Text>
              })
          }
         <Text>{this.props.error}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  counter: {
    padding: 30,
    alignSelf: 'center',
    fontSize: 26,
    fontWeight: 'bold',
  },
});