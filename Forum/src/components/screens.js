import { Navigation } from 'react-native-navigation';
import ListScreen from './listScreen';
import DetailScreen from './detailScreen';
import CreatePage from './createPage';

export default (store, Provider) => {
    Navigation.registerComponent('Forum.List', () => ListScreen, store, Provider);
    Navigation.registerComponent('Forum.Details', () => DetailScreen, store, Provider);
    Navigation.registerComponent('Forum.Create', () => CreatePage , store, Provider);
}

