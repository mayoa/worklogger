import React, { Component } from 'react';
import {
      TouchableOpacity,
      Button,
      StyleSheet,
      Text,
      View,
      TextInput
} from 'react-native';
import { FormLabel } from 'react-native-elements';
import { connect } from 'react-redux';
import { createPost } from "../actions";

export class CreatePage extends Component {
   constructor(props){
        super(props);

        this.state = {
            title: '',
            body: ' '
        }
     }



     render(){
         return (
             <View>
             <FormLabel>Title</FormLabel>
             <TextInput
                 style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                 onChangeText={(text) => this.setState({title: text})}
                 value={this.state.title}
             />
             <FormLabel>Body</FormLabel>
             <TextInput
                 style={{height: 60, borderColor: 'gray', borderWidth: 1}}
                 onChangeText={comment => this.setState({body: comment})}
                 value={this.state.body}
             />

             <TouchableOpacity
                 onPress={() => this.props.dispatch.onCreatePost(this.state.title,this.state.body)}
                 style={styles.submit}
               >
                 <Text>Submit</Text>
             </TouchableOpacity>
                 <Button
                     onPress={() => this.props.navigator.popToRoot({
                         animated: true,
                         animation: 'fade'
                       } )
                     }
                     />
         </View>) ;
     }

}


const mapDispatchToProps = (dispatch) => {
    return {
        onCreatePost: (subject, body) => dispatch(createPost(subject,body))
   };
};

export default connect(mapDispatchToProps)(CreatePage)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

});