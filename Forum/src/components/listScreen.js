import React, { Component } from 'react';
import {
    Button,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { fetchData } from '../actions';
import {getPostState } from "../reducers/dataReducer";

export class ListScreen extends Component {

    componentWillMount() {
        this.props.onRequestPost();
    }

    render() {

         const results = this.props.post.slice(0,5);

           if (!(this.props.loading && this.props.error)) {
                      results.map((each) => {
                          return(
                              <View>
                                  <Button
                                    onPress={() => this.props.navigator.push({
                                         screen: 'Forum.Create',
                                         title: 'Add More'
                                      })
                                    }
                                 />
                              <TouchableOpacity
                                  onPress={() => this.props.navigator.push({
                                      screen: 'Forum.Details',
                                      title: 'Details',
                                  })
                                  }>
                                  <Text>{each.title}</Text>
                              </TouchableOpacity>
                          </View>);
                      })

            }
              else{
                  return(
                    <View>
                        <Text>Error loading...</Text>
                        <Text>{this.props.error}</Text>
                    </View>
                  );
              }

          }

}//end of class

const  mapStateToProps = (state) => {
    return{
        post: getPostState(state),
        loading: state.loading,
        error:state.error,
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        onRequestPost: () => dispatch(fetchData())
   };
};


export default connect(mapStateToProps, mapDispatchToProps)(ListScreen)



const styles = StyleSheet.create({
  counter: {
    padding: 30,
    alignSelf: 'center',
    fontSize: 26,
    fontWeight: 'bold',
  },
});