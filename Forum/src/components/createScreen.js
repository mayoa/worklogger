import React, { Component } from 'react';
import * as appActions from '../actions';

export class CreatePage extends Component {
   constructor(props){
        super(props);

        this.state = {
            userId: 1,
            id: 1,
            title: ' ',
            body: ' ',
        };
     }

     render(){
         return <View>
             <FormLabel>Title</FormLabel>
             <TextInput
                 style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                 onChangeText={(text) => this.setState({title: text})}
                 value={this.state.title}
             />
             <FormLabel>Body</FormLabel>
             <TextInput
                 style={{height: 60, borderColor: 'gray', borderWidth: 1}}
                 onChangeText={comment => this.setState({body: comment})}
                 value={this.state.body}
             />

             <TouchableOpacity
                 onPress={() => this.props.onCreatePost()}
                 style={styles.submit}
             >
                 <Text>Submit</Text>
             </TouchableOpacity>
         </View>;
     }

}


const post = () => [{
     userId: 1,
     id: 1,
     title: {this.props.title},
     body: {this.props.body}
   }
]

const mapDispatchToProps = (dispatch) => {
    return {
        onCreatePost: () => dispatch(appActions.createData(post))
   };
};

export default connect(mapDispatchToProps)(CreatePage)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

});