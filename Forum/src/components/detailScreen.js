import React, { Component } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import {getDetails, getSelectedId} from "../reducers/dataReducer";
import { fetchDetails } from "../actions";

export class DetailScreen extends Component {

    componentWillMount() {
        this.props.fetchDetails(this.props.selectedId);
    }


  render() {
    return (
      <View>
           <Text>{this.props.details.id}{"\n"}
           Title: {this.props.details.title}{"\n"}
           Description: {this.details.body} </Text>
          <Button
              onPress ={() => this.props.navigator.popToRoot({
                  animated: true,
                  animationType: 'fade',
                  })
              }
              title={"Back"}
              />
      </View>
    );
  }
}

const  mapStateToProps = (state) => {
    return{
        details: getDetails(state),
        selectedId: getSelectedId(state),
        loading: state.loading,
        error:state.error,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDetails: (id) => dispatch(fetchDetails(id))
   };
};

export default connect(mapStateToProps, mapDispatchToProps) (DetailScreen)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

});