import React from 'react';
import {StyleSheet, Text,TextInput, View, Button, TouchableOpacity, Picker} from 'react-native';
import {StackNavigator, NavigationActions } from 'react-navigation';
import {FormLabel} from 'react-native-elements';

import {LogForm} from "./LogForm";



const Log = ({log,remove}) =>{
     return(
         <View>

             <Text>{log.Duration + "h"}</Text>
             <Text>{log.Project + "-" + log.Remarks}</Text>
             <TouchableOpacity
                  onPress={() => {remove(log.key)}}
                  >
                 <Text>Delete</Text>
             </TouchableOpacity>

     </View>);
};

const LogList = ({logs,remove}) =>{
    const logNode = logs.map((log,index) =>{
        return (<Log log={log} key={index} remove={remove} />);
    });
    return(
        <View>
            {logNode}
        </View>
    );

};

class WorkLogger extends React.Component{

     static navigationOptions = {
      title: 'Work Logger'
    };

    constructor(props){
        super(props);

        this.state = {
            workLogs: [],
            result: 0
        };

     }

      addLog = (time,task,comment) => {
             this.setState({
                 workLogs: this.state.workLogs.concat([{['Duration']: time, ['Project']: task, ['Remarks']: comment}])
             });


      }

      handleRemove = (id) => {

            const remainder = this.state.workLogs.filter((log) => {
                if(log.id  !== id){
                 return log;
                }
            });

            this.setState({
                  workLogs: remainder
            });
     };



     render() {

         return (
            <View>
               <LogForm addLog={this.addLog.bind(this)} />
               <LogList logs={this.state.workLogs} remove={this.handleRemove.bind(this)} />
            </View>
         );
     }

}


const WorkLoggerApp = StackNavigator({
    Home: { screen: WorkLogger },

});

//main class
export default class App extends React.Component {
  render() {
      return <WorkLoggerApp />;
  }
}

//Stylesheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
      justifyContent: 'center',
    alignItems: 'center'

  },
    submit: {
    alignItems: 'center',
    backgroundColor: '#1d8dff',
    padding: 10
  },
});


