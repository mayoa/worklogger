import React from 'react';
import {StyleSheet, Text,TextInput, View, Button, TouchableOpacity, Picker} from 'react-native';
import {FormLabel} from 'react-native-elements';

export class LogForm extends  React.Component{

    constructor(props){
        super(props);

        this.state = {
            duration: ' ',
            project: ' ',
            remarks: ' ',
        };

     }

     render(){
         return <View>
             <Text>Total Log Hours: {this.state.result} hours</Text>
             <FormLabel>Duration</FormLabel>
             <TextInput
                 style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                 keyboardType={'numeric'}
                 onChangeText={(num) => this.setState({duration: num})}
                 value={this.state.duration}
             />

             <FormLabel>Project</FormLabel>
             <Picker
                 onValueChange={(itemValue) => this.setState({project: itemValue})}
                 selectedValue={this.state.project}>
                 <Picker.Item label="Choose Project"/>
                 <Picker.Item label="Hydra" value="Hydra"/>
                 <Picker.Item label="Dragon MY" value="Dragon MY"/>
                 <Picker.Item label="Dragon P2P" value="Dragon P2P"/>
             </Picker>
             <FormLabel>Remarks</FormLabel>
             <TextInput
                 style={{height: 60, borderColor: 'gray', borderWidth: 1}}
                 onChangeText={comment => this.setState({remarks: comment})}
                 value={this.state.remarks}
             />

             <TouchableOpacity
                 onPress={() => {
                     return this.props.addLog(this.state.duration, this.state.project, this.state.remarks);
                 }}
                 style={styles.submit}
             >
                 <Text>Submit</Text>
             </TouchableOpacity>
         </View>;
     }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'

    },
    submit: {
        alignItems: 'center',
        backgroundColor: '#1d8dff',
    padding: 10
  },
});
