import React  from 'react';
import {StyleSheet, Text,TextInput, View, Button, TouchableOpacity} from 'react-native';
import {StackNavigator} from 'react-navigation';


export default class WorkLog extends React.Component{

  static navigationOptions = {
       title: 'Work Logger'
    };


  render (){
      return(
        <View>
            <Text>Duration: {this.props.duration}</Text>
            <Text>Project: {this.props.project}</Text>
            <Text>Remarks: {this.props.remarks}</Text>
        </View>
      );
  }
}





const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
      justifyContent: 'center',
    alignItems: 'center'

  },
});